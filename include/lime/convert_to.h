/**
 * Copyright (c) 2013, GOPS. Some rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *   - Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *   - Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *   - Neither the name of the Nokia Siemens Networks nor the names of its
 *     contributors may be used to endorse or promote products derived from
 *     this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
 * OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * Created on:  31-08-2013
 * Author:      gracjan.olbinski@gmail.com (Gracjan Olbinski)
 */

#pragma once
#ifndef LIME_CONVERTTO_HPP_
#define LIME_CONVERTTO_HPP_

#include <string>
#include <type_traits>
#include <boost/optional.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/numeric/conversion/cast.hpp>

namespace lime
{
namespace detail
{

template <bool AreSame, bool AreArithmetic, bool Bool2Str>
struct conversion;

template <bool AreSame, bool AreArithmetic>
struct conversion<AreSame, AreArithmetic, true> {
  template <typename To, typename From>
  static boost::optional<To> convert_to(From from) {
    return boost::optional<To>(from ? "true" : "false");
  }
};  // struct conversion<AreSame, AreArithmetic, true>

template <bool AreArithmetic>
struct conversion<true, AreArithmetic, false> {
  template <typename To, typename From=void>
  static boost::optional<To> convert_to(To from) {
    return boost::optional<To>(from);
  }
};  //  struct conversion<true, AreArithmetic, false>

template <>
struct conversion<false, false, false> {
  template <typename To, typename From>
  static boost::optional<To> convert_to(From from) {
    try {
      To converted = boost::lexical_cast<To>(from);
      return boost::optional<To>(converted);
    } catch ( ... ) {
      return boost::optional<To>();
    }
  }
};  // struct conversion<false, false, false>

template <>
struct conversion<false, true, false> {
  template <typename To, typename From>
  static boost::optional<To> convert_to(From from) {
    try {
      To converted = boost::numeric_cast<To>(from);
      return boost::optional<To>(converted);
    } catch ( ... ) {
      return boost::optional<To>();
    }
  }
};  // struct conversion<false, true, false>
}  // namespace detail

template <typename To, typename From>
inline boost::optional<To> convert_to(From from) {
  const bool same = std::is_same<To, From>::value;
  const bool arithmetic = (std::is_arithmetic<To>::value &&
    std::is_arithmetic<From>::value && !std::is_pointer<To>::value);
  const bool bool2str = (std::is_same<bool, From>::value &&
    (std::is_same<std::string, typename std::remove_const<
    typename std::remove_reference<To>::type>::type>::value ||
    std::is_same<const char*, To>::value));
  return detail::conversion<same, arithmetic, bool2str>::
    template convert_to<To, From>(from);
}

}  // namespace lime

#endif  // LIME_CONVERTTO_HPP_




