#
# Copyright (c) 2013, GOPS. Some rights reserved.
# Distributed under the MIT License.
#

cmake_minimum_required(VERSION 2.8)
project(LIBLIME)

option(LIBLIME_BUILD_TESTS "Build unit tests." ON)

if(${CMAKE_CXX_COMPILER_ID} MATCHES GNU)
  include(CheckCXXCompilerFlag)
  CHECK_CXX_COMPILER_FLAG(-std=c++11 HAVE_STD11)
  if(HAVE_STD11)
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11 -Wall -Werror -pedantic")
  else()
    message(FATAL_ERROR "No advanced standard C++11 support")
  endif()
elseif(${CMAKE_CXX_COMPILER_ID} MATCHES Clang)
  include(CheckCXXCompilerFlag)
  CHECK_CXX_COMPILER_FLAG(-std=c++11 HAVE_STD11)
  if (HAVE_STD11)
    if (CPP-NETLIB_DISABLE_LIBCXX)
      set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11 -Wall -Werror -pedantic")
    else()
      set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11 -stdlib=libc++ -Wall -Werror -pednatic")
      set(CMAKE_CXX_LINK_FLAGS "${CMAKE_CXX_LINK_FLAGS} -stdlib=libc++")
    endif()
  else()
    message(FATAL_ERROR "No C++11 support for Clang version. Please upgrade Clang to a version supporting C++11.")
  endif()
endif()

set(LIBLIME_INCLUDE_DIRS ${CMAKE_CURRENT_SOURCE_DIR}/include)
set(LIBLIME_TEST_DIR     ${CMAKE_CURRENT_SOURCE_DIR}/test)
set(RUNTIME_OUTPUT_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR})

message(STATUS "C++ Compiler ID: ${CMAKE_CXX_COMPILER_ID}")
message(STATUS "C++ Flags: ${CMAKE_CXX_FLAGS} link flags: ${CMAKE_CXX_LINK_FLAGS}")

if(LIBLIME_BUILD_TESTS)
  add_subdirectory(${LIBLIME_TEST_DIR}/deps/gmock)
  set(GTEST_ROOT         ${LIBLIME_TEST_DIR}/deps/gtest)
  set(GTEST_INCLUDE_DIRS ${GTEST_ROOT}/include)
  set(GTEST_LIBRARIES    gtest gtest_main)

  set(GMOCK_ROOT         ${LIMLIME_TEST_DIR}/deps/gmock)
  set(GMOCK_INCLUDE_DIRS ${GTEST_INCLUDE_DIRS} ${GMOCK_ROOT}/include)
  set(GMOCK_LIBRARIES    ${GTEST_LIBRARIES} gmock gmock_main)
  add_subdirectory(${LIBLIME_TEST_DIR}/tests)
endif()
  
