/****************************************************************************
 * Copyright (c) 2013, GOPS. Some rights reserved.
 *
 * Author:      gracjan.olbinski@gmail.com
 * Created:     2013-09-08
 *
 * Code covered by the MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 ****************************************************************************/

#include <gtest/gtest.h>
#include "lime/convert_to.h"

namespace lime {
namespace tests {

struct convert_to_should : public ::testing::Test {

};  // test fixture convert_to_should

TEST_F(convert_to_should, convert_true_boolean_to_string) {
  boost::optional<std::string> converted(lime::convert_to<std::string>(true));
  ASSERT_EQ("true", *converted);
}

TEST_F(convert_to_should, convert_false_boolean_to_string) {
  boost::optional<std::string> converted(lime::convert_to<std::string>(false));
  ASSERT_EQ("false", *converted);
}

}  // namespace tests
}  // namespace lime
